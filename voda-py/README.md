# Webová Python aplikace pro Voda Portabo

V této části aplikace se nachází vše potřebné pro vytvoření webové aplikace. Dockerfile v této složce zajistí, že webová část aplikace bude vytvořena jako Docker kontejner s potřebnými funkcemi a nainstalovanými knihovnami.

Ve složce **app** se nachází konfigurační soubory pro flask, nginx a uwsgi, požadavky na knihovny potřebné pro správný běh aplikace, schéma pro GraphQL a přístupový bod aplikace. 

Ve složce **app/app** je již veškerý kód aplikace. Navíc obsahuje další složky, které v sobě mají html Jinja šablony a statické prvky jako obrázky a kaskádové styly.
