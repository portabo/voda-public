var map = L.map('mapid').setView([50.492, 13.9], 9);
map._layersMaxZoom = 19;
let marker;
var markers = new L.markerClusterGroup();
var markerClusters = L.markerClusterGroup();
var vse = new L.markerClusterGroup();map.addLayer(vse);
var such = new L.markerClusterGroup();map.addLayer(such);
var norm = new L.markerClusterGroup();map.addLayer(norm);
var nelz = new L.markerClusterGroup();map.addLayer(nelz); 
var vypn = new L.markerClusterGroup();map.addLayer(vypn);
var spa1 = new L.markerClusterGroup();map.addLayer(spa1);
var spa2 = new L.markerClusterGroup();map.addLayer(spa2);
var spa3 = new L.markerClusterGroup();map.addLayer(spa3);

var Stadia_Outdoors = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 20,
    layers: [ markers,such,norm,spa1,spa2,spa3],
    attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
    }).addTo(map);
    // pridani ikonek ruzneho tvaru nebo barev
    LeafIcon = L.Icon.extend({
        options: {
            iconSize:     [38, 38],
        }
    });

var popup = L.popup();

	function onMapClick(e) {
		popup
			.setLatLng(e.latlng)
			.setContent("Pozice na mapě " + e.latlng.toString())
			.openOn(map);
	}

	map.on('click', onMapClick);

//CZ042geoData  definovano v CZ042.js kde jsou ulozeny geodata ohledne uzemi Usteckeho Kraje NUTS CZ042
L.geoJson(CZ042geoData,{style:{color: "#867979", opacity:0.9}}).addTo(map);
L.control.layers(overlays,null,{collapsed:false,position: 'topleft',autoZIndex:true}).addTo(map);
