from flask import Blueprint, render_template, request, jsonify
from .models import VodaFull, db, Hlasice
from ariadne import gql

from ariadne import load_schema_from_path, make_executable_schema, \
    graphql_sync, snake_case_fallback_resolvers, ObjectType
from ariadne.constants import PLAYGROUND_HTML
from .resolvers import listHlasice_resolver, getHlasic_resolver

# Spojení resolverů s Query funkcemi
query = ObjectType("Query")
query.set_field("listHlasice", listHlasice_resolver)
query.set_field("getHlasic", getHlasic_resolver)

# Načtení schematu ze souboru
type_defs = gql(load_schema_from_path("schema.graphql"))
schema = make_executable_schema(
    type_defs, query, snake_case_fallback_resolvers
)

# Vytvoření blueprintu pro API
api_bp = Blueprint(
    'api_bp', __name__,
    template_folder='templates',
    static_folder='static'
)

# Funkce pro zobrazení stránky o API
@api_bp.route('/api', methods=['GET'])
def api():

    return render_template(
        'api.html',
        curpage="api",
        title="Portabo Voda API",
        description="Popsáno API projektu Portabo Voda společně s odkazem na API"
    )

# Funkce pro zobrazení defaultního HTML playgroundu, ve kterém lze prozkoumávat data skrze GrpahQL
@api_bp.route("/graphql", methods=["GET"])
def graphql_playground():
    return PLAYGROUND_HTML, 200

# Pokud sena tu stejnou stránku budou dotazovat přes POST, pošleme jim odpověď v podobě JSONu
@api_bp.route("/graphql", methods=["POST"])
def graphql_server():
    data = request.get_json()
    success, result = graphql_sync(
        schema,
        data,
        context_value=request
    )
    status_code = 200 if success else 400
    return jsonify(result), status_code