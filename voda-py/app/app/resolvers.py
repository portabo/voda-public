from .models import Hlasice, VodaFull
from . import db
from ariadne import convert_kwargs_to_snake_case

# Resolver pro query na seznam hlásičů
def listHlasice_resolver(obj, info):
    try:
        # Použije funkci to_dict() pro každý nalezený hlásič
        hlasice = [hlasic.to_dict() for hlasic in db.session.query(VodaFull).order_by(VodaFull.ORP.asc()).all()] # Vytvoří slovník údajů pro každý nalezený hlásič
        payload = {
            "success": True,
            "hlasice": hlasice
        }
    except Exception as error:
        payload = {
            "success": False,
            "errors": [str(error)]
        }
    print(payload)
    return payload

# Resolver na určitý hlásič podle ORP
@convert_kwargs_to_snake_case # Změní zadané údaj na formát snake_case (oproti camelCase)
def getHlasic_resolver(obj, info, orp):
    try:
        hlasic = db.session.query(VodaFull).get(orp)
        payload = {
            "success": True,
            "hlasic": hlasic.to_dict()
        }

    except AttributeError:
        payload = {
            "success": False,
            "errors": [f"Hlásič s ORP {orp} nenalezen"]
        }
    print(payload)
    return payload
