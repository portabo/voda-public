from . import db
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import ForeignKey, text
from sqlalchemy.orm import relationship
from sqlalchemy.dialects import mysql
from datetime import datetime

# Nebudeme tvořit celou databázi, pouze si předpřipravíme pár tříd, které budeme používat
# Ty následně spojíme přes automap_base a prepare
Base = automap_base()

# Třída pro Hlásiče
class Hlasice(Base): # Hlasice nejspíš ani nepoužíváme

    __tablename__ = "hlasice"
    __table_args__ = {'extend_existing': True}

    # Email je v databází psán s pomlčkou a pak se na něho nelze odkazovat přes SQLAlchemy, přepíšeme
    def to_dict(self):
        return {
            "orp": self.orp,
            "nazev": self.nazev,
            "obec": self.obec,
            "tok": self.tok,
            "topic_cidla": self.topic_cidla,
            "poznamka": self.poznamka,
            "stav_profilu": self.stav_profilu,
            "url_foto": self.url_foto,
            "gps_souradnice": self.GPS_souradnice,
            "srazkomer": self.Srazkomer,
            "prutokomer": self.Prutokomer,
            "nulovy_stav": self.nulovy_stav,
            "aktivni": self.aktivni
        }

# Třída pro pohled VodaFull
class VodaFull(Base):
    __tablename__ = "vodaFull"
    __table_args__ = {'extend_existing':True, 'autoload':True, 'autoload_with':db.engine}

    # Pohled nemá primární klíč, vytvoříme si ho na již existujícím atributu ORP, který je unikátní
    ORP = db.Column(
        "ORP",
        db.String(10),
        primary_key=True
    )

    # Funkce pro GraphQL, část v uvozovkách musí být VŽDY malými písmeny
    def to_dict(self):
        return {
            "orp": self.ORP,
            "nazev": self.Nazev,
            "obec": self.Obec,
            "tok": self.Tok,
            "topic_cidla": self.topic_cidla,
            "poznamka": self.Poznamka,
            "url_foto": self.url_foto,
            "lat": self.lat,
            "lon": self.lon,
            "srazkomer": self.Srazkomer,
            "prutokomer": self.Prutokomer,
            "nulovy_stav": self.nulovy_stav,
            "aktivni": self.aktivni,
            "hladina": self.hladina,
            "floodstagelevel0": self.FloodStageLevel0,
            "floodstagelevel1": self.FloodStageLevel1,
            "floodstagelevel2": self.FloodStageLevel2,
            "floodstagelevel3": self.FloodStageLevel3,
            "floodstagelevel4": self.FloodStageLevel4,
            "hydroordernum": self.HydroOrderNum,
            "riverdistance": self.RiverDistance,
            "zerogaugeelevation": self.ZeroGaugeElevation,
            "uroven": self.uroven
        }

# Naklonujeme existující databázi, spojíme ji s již předpřipravenými třídami
Base.prepare(db.engine,reflect=True)