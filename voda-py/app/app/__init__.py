"""Inicializace Flask aplikace"""
from flask import Flask, url_for, has_request_context, request
from flask_sqlalchemy import SQLAlchemy
import logging
from logging.config import dictConfig

# Logger chyb, neukládáme do souboru
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s')
handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)

logger = logging.getLogger(__name__)
logger.addHandler(handler)
logger.handlers.extend(logging.getLogger("wsgi.error").handlers)

# Inicializace SQLAlchemy
db = SQLAlchemy()

# Funkce pro inicializaci Flasku
def init_app():
    app = Flask(__name__, instance_relative_config=False) # Vytvoření flask instance
    app.config.from_object('config.DevConfig') # Načtení nastavení ze souboru

    # Inicializace databáze s aplikací
    db.init_app(app)

    with app.app_context():
        from . import routes, api # Import souborů s blueprinty

        # Zaregistrování blueprintů
        app.register_blueprint(routes.main_bp)
        app.register_blueprint(api.api_bp)

        # Vytvoření­ databáze z modelů
        db.create_all()
        
        return app
