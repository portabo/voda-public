from sqlalchemy.sql import text

# Vytvoření dlouhého příkazu s dvěmi proměnnými "inter" a "orp"
# Najde data výšky hladiny za posledních inter dnů pro hlásič orp
    
query_hladina = text("select ORP,time, if(nulovy_stav,nulovy_stav- coalesce(json_extract(payload,'$.Distance')+0,0),coalesce(json_extract(payload,'$.Distance')+0,0))  as hladina,"
                "DATE_FORMAT(time, '%Y-%m-%dT%H:%i:%s') as timeformat"
                " from mqttentries right join hlasice on mqttentries.topic=concat('/voda/',hlasice.orp)"
                " where time>DATE_SUB(:now,interval :inter day) and ORP = :orp;"
)

    # Nalezení posledního záznamu od daného čidla
query_max_time = text("select max(time)"
                " from mqttentries right join hlasice on mqttentries.topic=concat('/voda/',hlasice.orp)"
                " where ORP = :orp;"
)


    # Nalezení posledního záznamu od daného čidla
query_count = text("select count(*)"
                " from mqttentries right join hlasice on mqttentries.topic=concat('/voda/',hlasice.orp)"
                " where time>DATE_SUB(:now,interval :inter day) and ORP = :orp;"
)

query_count_SPA = text("select count(*)"
                " from mqttentries right join hlasice on mqttentries.topic=concat('/voda/',hlasice.orp)"
                " where time>DATE_SUB(:now,interval :inter day) and ORP = :orp"
                " and if(nulovy_stav,nulovy_stav - coalesce(json_extract(payload,'$.Distance')+0,0),coalesce(json_extract(payload,'$.Distance')+0,0)) > :SPAlow"
                " and if(nulovy_stav,nulovy_stav - coalesce(json_extract(payload,'$.Distance')+0,0),coalesce(json_extract(payload,'$.Distance')+0,0)) < :SPAhigh;"
)

    # Najde záznamy baterky za posledních 90 dnů
query_battery = text("select ORP,time, json_extract(payload,'$.battery') as battery,"
                "DATE_FORMAT(time, '%Y-%m-%dT%H:%i:%s') as timeformat"
                " from mqttentries right join hlasice on mqttentries.topic=concat('/voda/',hlasice.orp)"
                " where time>DATE_SUB(:now,interval :inter day) and ORP = :orp;"
)