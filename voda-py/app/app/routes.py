from flask import Blueprint, render_template, send_from_directory, current_app
import os, time
from datetime import datetime

from .queries import query_battery, query_count, query_hladina, query_max_time
from .models import VodaFull, db

# Vytvoření blueprintu pro aplikaci
main_bp = Blueprint(
    'main_bp', __name__,
    template_folder='templates',
    static_folder='static'
)


# Úvodní stránka obsahující pouze navigační menu a mapu
@main_bp.route('/', methods=['GET'])
def home():
 
    # Vrátí z databáze všechny hlásiče podle pohledu VodaFull
    hlasice = db.session.query(VodaFull).order_by(VodaFull.ORP.asc()).all()

    # Vygeneruje stránku podle přiloženého templatua pošle ji další argumenty
    return render_template(
        'home.html',
        hlasice=hlasice, # Stránce pošleme hlásiče z databáze
        curpage="home", # Označení nynější stránky pro zvýraznění v navbaru
        title="Portabo Voda",
        description="Voda hlásiče"
    )

# Funkce pro zobrazení všech aktivních hlásičů
@main_bp.route('/stanice', methods=['GET'])
def hlasice():
 # Slovník okresů
    okresy = {
        'Bílina':[],'Děčín':[],'Kadaň':[],'Chomutov':[],'Litomeřice':[],'Litvínov':[],'Lovosice':[],'Louny':[],
        'Most':[],'Podbořany':[],'Roudnice nad Labem':[],'Rumburk':[],'Teplice':[],'Ústí nad Labem':[],'Varnsdorf':[],'Žatec':[]
    }
    orp_to_okres = {
        'BI':'Bílina','DE':'Děčín','KA':'Kadaň','CH':'Chomutov','LI':'Litomeřice','LT':'Litvínov','LO':'Lovosice','LY':'Louny',
        'MO':'Most','PO':'Podbořany','RL':'Roudnice nad Labem','RU':'Rumburk','TE':'Teplice','UL':'Ústí nad Labem','VA':'Varnsdorf','ZA':'Žatec'
    }

    hlasice = db.session.query(VodaFull).order_by(VodaFull.ORP.asc()).all()
    pocetHlasicu = db.session.query(VodaFull).order_by(VodaFull.ORP.asc()).count()

    i = 0
    delitel = []
    for hlasic in hlasice:
        ORP = hlasic.ORP[0:2]
        okresy[orp_to_okres[ORP]].append(hlasic)
        if (i > pocetHlasicu/2 and delitel==[]):
            delitel.append(orp_to_okres[ORP])
        i += 1
        # print(okres)

    # print(okresy)

    print("Delitel: {}".format(delitel))

    # Vygeneruje stránku podle přiloženého templatua pošle ji další argumenty
    return render_template(
        'profilyHlasicu.html',
        curpage="profily", # Označení nynější stránky pro zvýraznění v navbaru
        pocetHlasicu=pocetHlasicu,
        hlasice=okresy,
        delitel=delitel,
        title="Portabo Voda Hlásiče",
        description="Seznam všech používaných hlásičů"
    )

# Funkce pro zobrazení jednoho určitého hlásice podle ORP
@main_bp.route('/stanice/<orp>', methods=['GET'])
def hlasic(orp):
    start = time.time()

    # Pokusí se vyhledat hlásič podle zadaného ORP, pokud neexistuje, tak vypíše chybovou hlášku.
    hlasic = db.session.query(VodaFull).get_or_404(orp, description="Hlásič {} neexistuje.".format(orp))
   
    # Najít historii hladiny za posledních 28 dnů
    graph = db.session.execute(query_hladina,{"now":datetime.now(),"inter":30,"orp":orp})
    graphCount = db.session.execute(query_count,{"now":datetime.now(),"inter":30,"orp":orp}).first()[0]
    # Najít historii hladiny za posledních 24 hodin
    graph24 = db.session.execute(query_hladina,{"now":datetime.now(),"inter":1,"orp":orp})
    graphCount24 = db.session.execute(query_count,{"now":datetime.now(),"inter":1,"orp":orp}).first()[0]
    # Najít historii baterky za poslední tři měsíce
    graphbattery = db.session.execute(query_battery,{"now":datetime.now(),"inter":90,"orp":orp})
    graphBatteryCount = db.session.execute(query_count,{"now":datetime.now(),"inter":90,"orp":orp}).first()[0]
    
    print(f"Datum teď: {datetime.now()}")
    print(f"Proběhly tři kvery a zabralo to {time.time()-start} vteřin")
    start = time.time()
    
    print(f"Graph24: {graphCount24}, graph: {graphCount}, graph Battery: {graphBatteryCount}")
    # Pokud nenalezne žádné záznamy, tak provede stejné příkazy, ale nyní hledá časový úsek od posledního záznamu hlásiče
    if graphCount24 == 0:
        maxdate = db.session.execute(query_max_time,{"orp":orp}).first()[0]
        print(f"Maxdate: {maxdate}")
        print(f"Maxdate: {maxdate.strftime('%Y-%m-%d %H:%M:%S')}")
        graph24 = db.session.execute(query_hladina,{"now":maxdate.strftime('%Y-%m-%d %H:%M:%S'),"inter":1,"orp":orp,"lim":10000})
        if graphCount == 0:
            graph = db.session.execute(query_hladina,{"now":maxdate.strftime('%Y-%m-%d %H:%M:%S'),"inter":30,"orp":orp,"lim":10000})
        
    
    print(f"Proběhla podmínka prázdnoty a zabralo to {time.time()-start} vteřin")
    start = time.time()

    dataGraf = {}
    dataGraf24 = {}
    dataGrafBattery = {}
    # Vytvoření slovníků podle předchozích dotazů, formát "čas":"hladina"
    for zaznam in graph24:
        # print(type(zaznam.hladina))
        if type(zaznam.hladina) == int or type(zaznam.hladina) == float:
            dataGraf24[zaznam.timeformat]=zaznam.hladina
    for zaznam in graph:
        if type(zaznam.hladina) == int or type(zaznam.hladina) == float:
            dataGraf[zaznam.timeformat]=zaznam.hladina
    for zaznam in graphbattery:
        if type(zaznam.battery) == str:
            dataGrafBattery[zaznam.timeformat]=zaznam.battery

    latest=datetime.strptime(max(dataGraf24), '%Y-%m-%dT%H:%M:%S')
    print((datetime.now() - latest).days)
    late = 0
    if (datetime.now() - latest).days > 1:
        late = 1

    print(f"Proběhlo uložení do slovníčků a zabralo to {time.time()-start} vteřin")
    start = time.time()
    
    
    # Nalezne minimální a maximální hodnoty baterky za poslední tři měsíce
    if graphBatteryCount != 0:
        batteryMin = min(dataGrafBattery, key=dataGrafBattery.get)
        batteryMax = max(dataGrafBattery, key=dataGrafBattery.get)
    else: # Pokud není žádný záznam baterky za poslední tři měsíce, tak nastavit na nuly, aby stránka nehlásila errory
        batteryMax = 0
        batteryMin = 0
        dataGrafBattery[0]=0 # V Jinja šabloně se hledá ve slovníku podle klíče (který bude 0)

    
    print(f"Proběhlo vybrání hlásiče a zabralo to {time.time()-start} vteřin")

    # Vygeneruje stránku podle přiloženého templatua pošle ji další argumenty
    return render_template(
        'eviListHlasice.html',
        curpage="profily", # Označení nynější stránky pro zvýraznění v navbaru
        dataGraf=dataGraf, # Slovník dat pro měsíční graf
        dataGraf24=dataGraf24, # Slovník dat pro 24 hodinový graf
        dataGrafBattery=dataGrafBattery, # Slovník dat pro záznamy z baterie za poslední tři měsíce
        latest=latest, # Poslední naměřený údaj
        late=late, # Příznak jeslti máme neaktuální data
        batteryMin=batteryMin,
        batteryMax=batteryMax,
        hlasic=hlasic, 
        title="Portabo Voda Hlásič",
        description="Evidenční list profilu hlásiče"
    )

# Funkce pro zobrazení stránky O projektu
@main_bp.route('/oprojektu', methods=['GET'])
def oprojektu():

    # Vygeneruje stránku podle přiloženého templatu a pošle ji další argumenty
    return render_template(
        'oprojektu.html',
        curpage="oprojektu", # Označení nynější stránky pro zvýraznění v navbaru
        title="Portabo Voda O Projektu",
        description="Popis projektu Portabo Voda"
    )

# Funkce pro poslání souboru uživateli
@main_bp.route('/download/<path:filename>', methods=['GET','POST'])
def download(filename):

    # Spojení cesty k aplikaci s názvem upload složky podle configu
    uploads = os.path.join(current_app.root_path, current_app.config['UPLOAD_FOLDER'])

    # Zaslání souboru(filename) z upload složky uživateli. as_attachment způsobí, že soubor nezobrazí, ale rovnou dá na stažení
    return send_from_directory(directory=uploads, path=filename,as_attachment=True)

# Funkce pro zobrazení stránky Odkazy
@main_bp.route('/odkazy', methods=['GET'])
def odkazy():

    # Vygeneruje stránku podle přiloženého templatua pošle ji další argumenty
    return render_template(
        'odkazy.html',
        curpage="odkazy", # Označení nynější stránky pro zvýraznění v navbaru
        title="Portabo Voda Odkazy",
        description="Vhodné odkazy spojené s Portabo Vodou"
    )

# Funkce pro zobrazení stránky Novinky
@main_bp.route('/novinky', methods=['GET'])
def novinky():

    # Vygeneruje stránku podle přiloženého templatua pošle ji další argumenty
    return render_template(
        'novinky.html',
        curpage="novinky", # Označení nynější stránky pro zvýraznění v navbaru
        title="Portabo Voda Novinky",
        description="Nové zprávy v projektu Portabo Voda"
    )