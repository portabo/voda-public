from app import init_app
from werkzeug.middleware.proxy_fix import ProxyFix

app = init_app()
#app = ProxyFix(app)

if __name__ == "__main__":
    app.run(use_reloader=False)
