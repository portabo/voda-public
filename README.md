# Povodňový systém Ústeckého kraje

Webová aplikace napsaná pomocí Python microframeworku Flask s databází MariaDB, zabalené pomocí Dockeru. Slouží ke zvěřejňování přehledu o povodňových hlásičích pro veřejnost pomocí přehledných grafů a statistik. 

## Funkcionality webové aplikace

1. Hlavní stránka obsahuje mapu Ústeckého kraje, na které jsou vidět všechny uložené hlásiče. Ty jsou označeny ikonkou podle aktuální hladiny, popisem a odkazem na přidělenou stránku evidenčního profilu povodňového čidla. Na mapě lze vybrat hlásiče podle jejich aktuální hladiny (Normální stav, SPA1, SPA2, SPA3, KO, Vypnuto)
2. Seznam všech profilů, rozdělených podle okresu.
3. Jednotlivé stránky profilů, které obsahují statistiky ve formě tabulky, graf hladiny za posledních 24 hodin od posledního údaje, graf hladiny za poslední měsíc, graf úrovně baterie za poslední tři měsíce.
4. Stránka novinek
5. Stránka o projektu spojené s odpovědmi na časté otázky
6. Stránka užitečných odkazů
7. Veřejně přístupné API pomocí GraphQL

## Databáze

Databázi tvoří docker image MariaDB verze 10.5.9, obsahující následující tabulky:
1. Hlásiče - obsahuje informace o jednotlivých hlásičích
2. Kontakty - osobní údaje zodpovědných osob k hlásičům. Jedná se například o starosty, kteří chtějí vědět, kdy hladina stoupne na vyšší stupeň (SPA1-SPA3).
3. MQTTEntries - Zprávy zaslané od senzorů, obsahují informace o hladině, srážkách, teplotě vlhkosti a další.
4. CharakteristikaProfiluMěření - Přidružené informace k hlásičům, jako jsou například stupně SPA v centimetrech nebo také říční kilometr čidla.
5. vodaFull - Vytvořený view, který všechny tabulky spojuje a vypočítává údaje jako například aktuální hladinu.

Tyto tabulky se do databáze vloží automaticky společně s testovacími daty při prvním spuštění aplikace. Bližší informace k tabulkám se nachází ve složce *mysql-docker*.

## Spuštění aplikace

Celá aplikace je zabalena pomocí Dockeru, a proto má velmi jednoduché spuštění. Je zapotřebí mít nainstalovaný Docker a Docker-Compose na linuxovém stroji/serveru. 

Aplikace je navržená tak, aby ji stačilo stáhnout a v této složce spustit příkaz `docker-compose up`. Tím se spustí webová stránka s přidruženou databází a můžete ji najít na adrese svého stroje s portem 5999 (například localhost:5999). 

Tohle všechno zajišťuje soubor **docker-compose.yml**. Databázi vytvoří podle obrazu mariadb:10.5.9, nastaví ji heslo pro uživatele root, vytvoří databázi portabo a přepne na český čas. Pomocí volume propojí dvě složky s vnitřními složky kontejneru. To zajistí inicializaci databáze (propojení s */docker-entrypoint-initdb.d*) a zároveň persistenci databáze (propojením s */var/lib/mysql*).

Webová aplikace se vytvoří pomocí přiloženého Dockerfilu ve složce *voda-py*. Přidělí mu tajný klíč a také způsob jak se připojit do databáze pro použitý SQLAlchemy. Následně nastaví port pro připojení zvenku a propojí s databází. Český čas je již nastaven v Dockerfilu.

U databáze i aplikace se navíc nastaví příznak `restart: always`, který nám zajistí, že aplikace se vždy restartuje pokud by se z nějakého důvodu vypnula.