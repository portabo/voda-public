# Databáze MariaDB 10.5.9

Nachází se zde dvě složky, jedna pro inicializaci databáze a druhá pro ukládání databáze. 

Ve složce **mariadb-init** lze najít mysqldump databáze s testovacími daty. Ten se použije pro založení databáze a jejího naplnění ukázkovými daty. 

Ve složce **mariadb-data** se nic nenachází do té doby, než se aplikace spustí. Pak se do ní uloží celá mariadb databáze a bude v ní uchovávána. Tím se zaručí, že při vypnutí aplikace a Dockeru se data této databáze nevymažou. 

## Struktura databáze

### Hlásiče
* ORP - unikátní identifikátor ve formátu **PP-ČČ**, kde **P** je písmeno a **Č** je číslo. Písmena jsou většinou první dvě ze sloupce Nazev.
* Nazev - Název obce s rozšířenou působností
* Obec - Název konkrétní obce, ve které je hlásič umístěn
* GPS_souradnice - Souřadnice ve formátu "00,000000 00,000000"
* Stav_profilu - Hodnota Ano/Ne, ukazuje jeslti má hlásič záznam v tabulce CharakteristikaProfiluMěření
* Srazkomer - Hodnota Ano/Ne, ukazuje jestli byl hlásič nainstalován se srážkoměrem
* Prutokomer - Honodota Ano/Ne, ukazuje jestli byl hlásič nainstalován s průtokoměrem
* Poznamka - Poznámka k umístění čidla
* topic_cidla - Topic čidla v MQTT síti, které posílá údaje pro daný hlásič
* nulovy_stav - Hodnota, od které se musí odečíst naměřená vzdálenost čidlem, abychom získali výšku hladiny
* Tok - Název potoku či řeky
* aktivni - Příznak 0/1, podle kterého zobrazujeme na mapě aktuální stav, či "Vypnuto"
* url_foto - Odkaz na veřejnou stránku s fotografiemi umístění čidla

### Kontakty
* id - Unikátní identifikátor kontaktu
* codeORP - Shodný kód jako ORP u hlásičů.
* email - E-mailová adresa kontaktu
* telefon - Telefon, na který posíláme SMSky ohledně zvednutí hladiny
* jmeno - Jméno a příjmení kontaktu
* aktivni - Příznak 0/1, který značí jestli máme posílat kontaktu SMSky a Emaily či ne. Číslo 1 značí Ano.

### MQTTEntries
* id - Unikátní identifikátor
* time - Čas uložení zprávy do databáze
* topic - Téma zprávy, je ve formátu **/voda/ORP**, kde ORP odpovídá stejnojmenému kódu v tabulce Hlásičů
* payload - Veškerá data zprávy obsahující například vzdálenost, teplotu a vlhkost

### CharakteristikaProfiluMěřeníRV
* SiteGuid - Unikátní identifikátor umístění
* SiteIdent - Odpovídá ORP kódu v tabulce hlásičů
* DefaultLabel - Odpovídá sloupci Obec v tabulce hlásičů
* Wsg84Lon, Wsg84Lat - Zeměpisná šířka a výška
* GeoposCode - Název typu uvádění souřadnic pro následující záznamy
* GeoposLonX, GeoposLonY - Zeměpisná šířka a výška podle GeoposCode
* SiteOperator - Název kraje, ve kterém je hlásič umístěn
* WaterwayCode - kód řečiště
* HydroOrderNum - Číslo hydrologického pořadí
* RiverDistance - Říční kilometr
* RiverDistanceUnit - Jednotka říčního kilometru
* ZeroGaugeElevation - Výška umístění v metrech nad mořem
* ZeroGaugeElevationUnit - Jednotka výšky umístění
* FloodStageZone0 - SUCHO
* FloodStageZone1 - NORMAL
* FloodStageZone2 - SPA1
* FloodStageZone3 - SPA2
* FloodStageZone4 - SPA3
* FloodStageLevelUnit - Jednotka povodňových stupňů
* FloodStageLevel0 - Při jaké hladině dosáhne hlásič stavu SUCHO
* FloodStageLevel1 - Při jaké hladině dosáhne hlásič stavu NORMAL
* FloodStageLevel2 - Při jaké hladině dosáhne hlásič stavu SPA1
* FloodStageLevel3 - Při jaké hladině dosáhne hlásič stavu SPA2
* FloodStageLevel4 - Při jaké hladině dosáhne hlásič stavu SPA3
